-- INSERTION DES COLONNES LATITUDE ET LONGITUDE
ALTER TABLE ADDRESS ADD COLUMN longitude VARCHAR(255) NOT NULL
ALTER TABLE ADDRESS ADD COLUMN latitude VARCHAR(255) NOT NULL

-- REQUETE POUR AFFICHER LE CLIENT QUI A FAIT LE PLUS DE LOCATION

SELECT 
    last_name, 
    first_name, 
    address, 
    latitude, 
    longitude, 
    count(r.customer_id) AS nb
FROM customer c 
JOIN address a ON c.address_id = a.address_id 
JOIN rental r on r.customer_id = c.customer_id 
GROUP BY r.customer_id HAVING COUNT (r.customer_id)=( 
SELECT MAX(r.customer_id) 
FROM ( SELECT count(r.customer_id) AS nb
FROM customer c 
JOIN rental r on r.customer_id = c.customer_id 
GROUP BY r.customer_id));