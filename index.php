<?php
    require('vendor/autoload.php');
    use Symfony\Component\HttpClient\HttpClient;

    // Pamametre de la connexion
    $servername = "localhost";
    $username = "root";
    $password = "root";
    $database = "dataengineer";

    // creation de la connection
    $db = mysqli_connect($servername, $username, $password, $database);

    // check connection
    if(mysqli_connect_errno()) {
        $msg = "Database connection failed: ";
        $msg .= mysqli_connect_error();
        $msg .= " : " . mysqli_connect_errno();
        exit($msg);
    }


    // Attempt select query execution
    $req = "SELECT address_id, address, city, postal_code FROM address";
 
    // on envoie la requête
    $address = mysqli_query($db, $req);

    $httpClient = HttpClient::create();
    $url = 'https://nominatim.openstreetmap.org/search?q=';

    while($add = mysqli_fetch_assoc($address)) {
        $query = $url . $add['address'] . ' ' . $add['postal_code'] . ' ' .  $add['city'] . '&format=json';
        $address_id = $add['address_id'];

        // Appel de l'api pour recuperer la latitude et la longitude
        $response = $httpClient->request(
            'GET', 
            $query
        );

    
        $coord = json_decode($response->getContent());
        if (!empty($coord)) {
            $lat = $coord[0]->lat;
            $long = $coord[0]->lon;
    
            $sql = "UPDATE address SET latitude = $lat, longitude = $long WHERE address_id = $address_id";
            $result = mysqli_query($db, $sql);
        } else {
            echo "L'adresse est introuvable";
            echo "</br>";
        }
    }

    mysqli_close($db);
?>